email = {
	log = function(msg) end
}
local _loading = true
local to_reply = nil
local to_reply_name = nil
local T = minetest.get_translator("minetest_email")

if minetest.global_exists("chatplus") then
	email.log = chatplus.log

	function chatplus.on_old_inbox(inbox)
		if _loading then
			return
		end

		email.inboxes = inbox
	end
end

local postman = {
    initial_properties = {
        hp_max = 100,
        physical = true,
        collide_with_objects = true,
        collisionbox = {-0.3, 0.0, -0.3, 0.3, 1.7, 0.3},
		visual      = "mesh",
		mesh        = "character.b3d",
		visual_size = {x=1, y=1},
        textures = {"mailman.png"},
        spritediv = {x = 1, y = 1},
        initial_sprite_basepos = {x = 0, y = 0},
    },

    message = " ",
}

minetest.register_entity("minetest_email:mailman", postman)
local pos = { x = 1, y = 10, z = 30 }
local obj = minetest.add_entity(pos, "minetest_email:mailman", nil)

function postman:set_message(msg)
    self.message = msg
end

function postman:on_rightclick (hitter)
	email.show_inbox(hitter:get_player_name())
end

function postman:on_punch (hitter)
	if hitter and hitter:is_player() then
        if not minetest.check_player_privs(hitter, "build") then
			return 0
        end
    end
end


function email.init()
	email.inboxes = {}
	email.load()
	_loading = false
	if minetest.global_exists("chatplus") and chatplus.old_inbox then
		chatplus.on_old_inbox(chatplus.old_inbox)
	end
end

function email.load()
	local file = io.open(minetest.get_worldpath() .. "/email.txt", "r")
	if file then
		local from_file = minetest.deserialize(file:read("*all"))
		file:close()
		if type(from_file) == "table" then
			if from_file.mod == "email" and tonumber(from_file.ver_min) <= 1 then
				email.inboxes = from_file.inboxes
			else
				error("[Email] Attempt to read incompatible email.txt file.")
			end
		end
	end
end

function email.save()
	local file = io.open(minetest.get_worldpath() .. "/email.txt", "w")
	if file then
		file:write(minetest.serialize({
			mod = "email", version = 1, ver_min = 1,
			inboxes = email.inboxes }))
		file:close()
	end
end
minetest.register_on_shutdown(email.save)

function email.get_inbox(name)
	return email.inboxes[name] or {}
end

function email.clear_inbox(name)
	email.inboxes[name] = nil
	email.save()
end

minetest.register_on_joinplayer(function(player)
	local inbox = email.get_inbox(player:get_player_name())
	local pname = player:get_player_name()
	local inbox_to_read = 0

	for i = 1, #inbox do
		if inbox[i].read then
			inbox_to_read = inbox_to_read + 1
		end
	end
	if inbox_to_read > 0 then
		minetest.after(10, function()
			minetest.sound_play('notification', {
				to_player = pname,
				gain = 2.0,
			})
			minetest.chat_send_player(player:get_player_name(),
				"(" ..  inbox_to_read .. ") " .. T("You have mail!"))
		end)
	end
end)

function email.get_formspec(name)
	local inbox = email.get_inbox(name)
	local fs = ""

	function row(fs, c1, date, from, sbj)
		date = minetest.formspec_escape(date)
		from = minetest.formspec_escape(from)
		subject = minetest.formspec_escape(sbj)
		return fs .. ",#d0d0d0," .. table.concat({date, c1, from, sbj}, ",")
	end

	fs  = fs .. "tablecolumns[color;text;color;text;text]"
	fs  = fs .. "tableoptions[highlight=#ffffff33]"
	fs  = fs .. "table[0.5,0;11.25,7;inbox;"
	fs  = fs .. "#ffffff," .. T("Date") .. ",," .. T("From") .. "," .. T("Subject") ..""
	if #inbox == 0 then
		fs = row(fs, "#d0d0d0", "", "", T("Your MailBox is empty!"))
	else
		for i = 1, #inbox do
			local color = "#ffffff"
			if inbox[i].read then
				color = "#FFD700"
			end
			local subject_mail = inbox[i].sbj
			if #subject_mail > 45 then
				subject_mail = subject_mail:sub(45, #subject_mail) .. ".."
				-- fs = row(fs, color, "", "", subject_mail:sub(1, 44))
			end
			fs = row(fs, color, inbox[i].date, inbox[i].from, subject_mail:sub(1, 49))
		end
	end
	fs = fs .. "]"

	fs = fs .. "button[0.5,7.25;2,1;new_message;" .. T("New Message") .. "]"
	fs = fs .. "button[3.5,7.25;2,1;mark_read;" .. T("Mark as read") .. "]"
	fs = fs .. "button[6.5,7.25;2,1;clear;" .. T("Delete All") .. "]"
	fs = fs .. "button_exit[10,7.25;2,1;close;" .. T("Close") .. "]"
	return fs
end


function email.writemail_form(name,to,sbj)
	if to == nil then
		to = ''
	end

	if sbj == nil then
		sbj = ''
	end

    local title = " "
    local formspec = {
        "formspec_version[4]",
        "size[9,10]",
        "label[0.375,0.5;", minetest.formspec_escape(title), "]",
        "field[1,1.;5.25,0.8;to;" .. T("To (case sensitive)") .. ";"..tostring(to).."]",
		"field[1,2.25;5.25,0.8;sbj;" .. T("Subject") .. ";"..tostring(sbj).."]",
		"textarea[1,3.50;5.25,5;body;" .. T("Body") .. ";]",
        "button[1,9;5.25,0.8;send_mail;" .. T("Send") .. "]"
		-- "image[7,1.25;0.8,0.8;tooltip_ico.png]",
		-- "tooltip[7,1.25;5.25,0.8;Remember: This field is case sensitive.\nWrite EXACTLY the name of the player who you want to send this mail]"
    }
    return table.concat(formspec, "")
end

function email.read_mail_form(name,from,msg,sbj)
	if to == nil then
		to = ''
	end
    local title = " "
    local formspec = {
        "formspec_version[4]",
        "size[12,10]",
        "label[0.375,0.5;", minetest.formspec_escape(title), "]",
		"textarea[1,1.25;10.25,0.8;;" .. T("From") ..":" .. " " .. tostring(from) .. ";]",
		"textarea[1,2.50;10.25,0.8;;" .. T("Subject") ..":" .. " " .. tostring(sbj) .. ";]",
		"textarea[1,3.50;5.25,5;;;"..tostring(msg).."]",
		"button[1,8.25;2,1;reply;" .. T("Reply") .. "]",
		"button[5,8.25;2,1;delete_mail;" .. T("Delete") .. "]",
		"button[9,8.25;2,1;open_inbox;" .. T("Close") .. "]"
    }
    return table.concat(formspec, "")
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "minetest_email:write" then
        return
    end

    if fields.body and fields.to then
        local pname = player:get_player_name()
		fields.body = string.gsub(fields.body,"\n"," ")
		email.send_mail(pname, fields.to, fields.body, fields.sbj)

		minetest.sound_play('sent', {
            to_player = pname,
            gain = 2.0,
        })

		minetest.close_formspec(pname, "minetest_email:write", email.writemail_form(pname,nil))
    end
end)


function email.show_write_form(name, to,sbj)
    minetest.show_formspec(name, "minetest_email:write", email.writemail_form(name,to,sbj))
end

function email.show_read_form(name, from, msg,sbj)
    minetest.show_formspec(name, "minetest_email:read_mail_form", email.read_mail_form(name,from,msg,sbj))
end


function email.show_inbox(name, text_mode)
	if text_mode then
		local inbox = email.get_inbox(name)
		if #inbox == 0 then
			return true, T("Your inbox is empty!")
		else
			minetest.chat_send_player(name, #inbox .. " items in your inbox:")
			for i = 1, #inbox do
				local item = inbox[i]
				minetest.chat_send_player(name, i .. ") " ..item.date ..
					" <" .. item.from .. "> " .. item.msg)
			end
			return true, "End of mail (" .. #inbox .. " items)"
		end
	else
		local fs = "size[12,8]" .. email.get_formspec(name)
		minetest.show_formspec(name, "email:inbox", fs)

		return true, "Opened MailBox!"
	end

	return true
end

minetest.register_on_player_receive_fields(function(player,formname,fields)
	local inbox = email.inboxes[name]
	if fields.clear then
		local name = player:get_player_name()
		email.clear_inbox(name)
		minetest.chat_send_player(name, T("Inbox cleared!"))
		email.show_inbox(name)
	end

	if fields.new_message then
		local name = player:get_player_name()
		email.show_write_form(name)
	end

	if fields.reply then
		local name = player:get_player_name()
		local inbox = email.get_inbox(name)
		local subject_to_reply = " "
		if #inbox > 0 and tonumber(to_reply) and tonumber(to_reply)-1 > 0 then
			to_reply_name = tostring(inbox[tonumber(to_reply)-1].from)
			if tostring(inbox[tonumber(to_reply)-1].sbj) ~= "" then
				subject_to_reply = "RE: " .. tostring(inbox[tonumber(to_reply)-1].sbj)
			end
			if to_reply_name ~= nil then
				email.show_write_form(name,to_reply_name,subject_to_reply)
			end
		end
	end

	if fields.inbox then
		local name = player:get_player_name()
		local table_event = minetest.explode_table_event(fields["inbox"])
		local inbox = email.inboxes[name]
		if table_event then
			to_reply = tonumber(table_event.row)
			if tostring(table_event.type) == "DCL" and inbox ~= nil then
				if inbox[tonumber(to_reply)-1] ~= nil then
					inbox[tonumber(to_reply)-1].read = false
					email.save()
					email.show_inbox(name)
					email.show_read_form(name,inbox[tonumber(to_reply)-1].from, inbox[tonumber(to_reply)-1].msg, inbox[tonumber(to_reply)-1].sbj)
				end
			end
		end
	end

	if fields.mark_read then
		local name = player:get_player_name()
		local inbox = email.inboxes[name]
		if tonumber(to_reply) and tonumber(to_reply)-1 > 0 and inbox ~= nil then
			inbox[tonumber(to_reply)-1].read = false
			email.save()
			email.show_inbox(name)
			to_reply = nil
			to_reply_name = nil
		end
	end

	if fields.delete_mail then
		local name = player:get_player_name()
		local inbox = email.inboxes[name]
		if tonumber(to_reply) and tonumber(to_reply)-1 > 0 then
			table.remove(inbox, tonumber(to_reply)-1)
			email.save()
			email.show_inbox(name)
		end
	end

	if fields.open_inbox then
		local name = player:get_player_name()
		email.show_inbox(name)
	end
end)

function email.send_mail(name, to, msg, subject)
	email.log("Email - To: "..to..", From: "..name..", MSG: "..msg)
	if true then
		local mail = {
			date = os.date("%Y-%m-%d %H:%M"),
			from = name,
			msg = msg,
			read = true,
			sbj = subject}

		if email.inboxes[to] then
			table.insert(email.inboxes[to], mail)
		else
			email.inboxes[to] = { mail }
		end

		email.save()

		minetest.chat_send_player(to, name .. " " .. T("sent you mail!"))
		minetest.chat_send_player(name, T("Email sent!"))

		minetest.sound_play('notification', {
			to_player = to,
			gain = 2.0,
		})

		return true, "Message sent to " .. to
	else
		minetest.chat_send_player(name,"Player '" .. to .. "' does not exist")
		return false, "Player '" .. to .. "' does not exist"
	end
end

minetest.register_chatcommand("mailbox", {
	description = T("Use this command to see your Inbox. You can read and write mails with that"),
	func = function(name)
			return email.show_inbox(name, false)
	end
})


dofile(minetest.get_modpath("minetest_email") .. "/src/hud.lua")

email.init()