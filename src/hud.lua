local hudkit = dofile(minetest.get_modpath("minetest_email") .. "/src/hudkit.lua")
dofile(minetest.get_modpath("minetest_email") .. "/src/config.lua")
email.hud = hudkit()
function email.update_hud(player)
	local name = player:get_player_name()
	local inbox = email.get_inbox(name)
	local inbox_to_read = 0

	for i = 1, #inbox do
		if inbox[i].read then
			inbox_to_read = inbox_to_read + 1
		end
	end

	if inbox_to_read and inbox_to_read > 0 then
		if email.hud:exists(player, "email:text") then
			email.hud:change(player, "email:text", "text", inbox_to_read .. " Mail")
		else
			email.hud:add(player, "email:icon", {
				hud_elem_type = "image",
				name = "MailIcon",
				position = { x = 1, y = 1},
				offset = minetest_email_settings.position_icon_HUD_mail,		
				text="email_mail.png",
				alignment = {x = -1, y = -1},
				scale = {x = 3, y = 3},		
			})

			email.hud:add(player, "email:text", {
				hud_elem_type = "text",
				name = "MailText",
				position = { x = 1, y = 1},
				offset = minetest_email_settings.position_text_HUD_mail,	
				text= inbox_to_read .. " Mail",
				alignment = {x = -1, y = -1},
				scale = {x = 3, y = 3},
				number = "0xFFFFFF"		
			})
		end
	else
		email.hud:remove(player, "email:icon")
		email.hud:remove(player, "email:text")
	end
end
minetest.register_on_leaveplayer(function(player)
	email.hud.players[player:get_player_name()] = nil
end)
function email.update_all_hud()
	local players = minetest.get_connected_players()
	for _, player in pairs(players) do
		local name = player:get_player_name()
		if(minetest.get_modpath("arena_lib")) ~= nil then
			local arena = arena_lib.get_arena_by_player(player:get_player_name())
			if arena ~= nil then 
				email.hud:remove(player, "email:icon")
				email.hud:remove(player, "email:text")
			else
				email.update_hud(player)
			end
		end
	end
	minetest.after(5, email.update_all_hud)
end
minetest.after(5, email.update_all_hud)
