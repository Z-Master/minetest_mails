# Minetest Mails
Send messages as e-mails between players!

This mod can be used with a NPC (entity) that is the MailMan who will send your message to the players.
Also can be used by commands.

### Commands
here the list of commands:

`/mailbox`: Open the Inbox of the player


This mod is a fork of this [mod](https://github.com/rubenwardy/email) made by [rubenwardy](https://github.com/rubenwardy)


### How to use it?
To use this mod you only need to spawn the "mailmail" entity inside your world with this command in the chat:
`/spawnentity minetest_email:mailman`, this command will spawn your Postman at the exactly coords where you are when you type that command.

After that, you need simply right-click on this entity and will open your Inbox and then you are ready to write and read your mails:)

Are you far away from the postman and you need to open your inbox? Don't worry, simply type in the chat the command `/mailbox` and your inbox will be opened!

### Want to help?
Feel free to:
* open an [issue](https://gitlab.com/mrfreeman_works/minetest_mails/-/issues)
* submit a merge request. In this case, PLEASE, do follow milestones and my [coding guidelines](https://cryptpad.fr/pad/#/2/pad/view/-l75iHl3x54py20u2Y5OSAX4iruQBdeQXcO7PGTtGew/embed/). I won't merge features for milestones that are different from the upcoming one (if it's declared), nor messy code